# gps_clock

# Why?
- Tired of setting the microwave clock after a power outage?
- Struggling with setting up NTP?

This project can't really help with those, but I'm having fun with it for other reasons.
GPS depends on accurate synchronization of time. 
Every satilite contains an atomic clock. 
The time provided by GPS is accurate to something like 40 nanoseconds. 
A GPS module can spit out lots of interesting data including: time, heading, alt, speed, lat, etc.  


# Components:
- Arduino Nano MEGA328P (will also work with MBED by playing with Serial config).
- NEO-6M GPS module (Specifically GoouuuTech GT-U7, but any GPS module with a UART should work.)
- OLED screen SSD1306

# Connections:
- GPS module connected to Nano MCU UART (TX-->RX), 3.3V, GND
- OLED screen connected to Nano MCU i2c bus (SDA, SCL), 3.3V, GND

# Method:
0. Make physical connections
1. Install TinyGPS++ library https://github.com/mikalhart/TinyGPSPlus
2. Install Adafruit SSD1306 library https://github.com/adafruit/Adafruit_SSD1306
3. Compile/upload program to MCU


# Future work:
- Battery charge circuit
- Enclosure

