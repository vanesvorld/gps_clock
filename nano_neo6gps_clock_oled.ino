// Arduino GPS real time clock with NEO-6M GPS module

#include <TinyGPS++.h>           // Include TinyGPS++ library
#include <Adafruit_SSD1306.h>
#include <Wire.h>


#define WIRE Wire

TinyGPSPlus gps;
byte last_second;
char Time[]  = "00:00:00";
char Date[]  = "00/00/2000";


Adafruit_SSD1306 display = Adafruit_SSD1306(128, 32, &WIRE);

void setup(void) {
  Serial.begin(9600);
  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // Address 0x3C for 128x32
  
  display.clearDisplay();
  display.display();

  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);

  display.setCursor(0,0);
  display.print(Date);
  display.print(" ");
  display.println(Time);

  display.setCursor(0,0);
  display.display(); // actually display all of the above

}





void loop() {

  while (Serial.available() > 0) {

    if (gps.encode(Serial.read())) {

      if (gps.time.isValid()) {
        Time[0]  = gps.time.hour()   / 10 + 48;
        Time[1]  = gps.time.hour()   % 10 + 48;
        Time[3]  = gps.time.minute() / 10 + 48;
        Time[4]  = gps.time.minute() % 10 + 48;
        Time[6] = gps.time.second() / 10 + 48;
        Time[7] = gps.time.second() % 10 + 48;
      }

      if (gps.date.isValid()) {
        Date[0]  = gps.date.day()    / 10 + 48;
        Date[1]  = gps.date.day()    % 10 + 48;
        Date[3]  = gps.date.month()  / 10 + 48;
        Date[4]  = gps.date.month()  % 10 + 48;
        Date[8] =(gps.date.year()   / 10) % 10 + 48;
        Date[9] = gps.date.year()   % 10 + 48;
      }

      if(last_second != gps.time.second()) {
        last_second = gps.time.second();
        display.clearDisplay();

        display.setCursor(0,0);
        display.print(Date);
        display.print(" ");
        display.println(Time);

        display.setCursor(0, 10);
        display.print(gps.altitude.meters());
        display.print(" ");
        display.print(gps.speed.mps());
        display.print(" ");
        display.print(gps.satellites.value());
        display.print(" ");
        display.print(gps.course.deg());

        display.setCursor(0,20);
        display.print(gps.location.lat());
        display.print(" ");
        display.print(gps.location.lng());

        display.setCursor(0,0);
        display.display(); // actually display all of the above
      }

    }

  }

}